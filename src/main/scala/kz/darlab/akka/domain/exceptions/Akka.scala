package kz.darlab.akka.domain.exceptions


object DarErrorSystem extends ErrorSystem {
  override val system = "DAR"
}

object DarErrorCodes {


  case class INTERNAL_SERVER_ERROR(override val series: ErrorSeries,
                                   override val system: ErrorSystem) extends ErrorCode {
    override val code = 1
  }
}

object DarErrorSeries {

  case object CORE extends ErrorSeries {
    override val series = 0
  }

}
