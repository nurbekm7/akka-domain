package kz.darlab.akka.domain.serializers

import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, LocalDate}
import org.json4s.{CustomSerializer, JString}

trait DateTimeSerializer {

  /**
    * Define custom date time format
    */
  val dateTimeFormat : String

  def dateTimeFormatter = DateTimeFormat.forPattern(dateTimeFormat)

  class DateTimeSerializer extends CustomSerializer[DateTime](format => (
    {
      case JString(s) => DateTime.parse(s, dateTimeFormatter)
    },
    {
      case dateTime: DateTime => JString(dateTime.toString(dateTimeFormatter))
    }
  ))

  // Date Serializer
  val dateFormat = "yyyy-MM-dd"
  def dateFormatter = DateTimeFormat.forPattern(dateFormat)

  class DateSerializer extends CustomSerializer[LocalDate](format => (
    {
      case JString(s) => LocalDate.parse(s, dateFormatter)
    },
    {
      case dateTime: LocalDate => JString(dateTime.toString(dateFormatter))
    }
  ))
}
