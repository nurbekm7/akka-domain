package kz.darlab.akka.domain.serializers

import kz.darlab.akka.domain.core._




trait CoreSerializer {

  val coreTypeHints = List(
    classOf[DomainMessage[_]],
    classOf[Request[_]],
    classOf[Response[_]],
    classOf[Accepted],
    classOf[SeqEntity[_]],
    classOf[Event[_]]
  )

}
