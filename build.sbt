organization:= "kz.darlab"

name := "akka-domain"

version := "1.0.0"

scalaVersion := "2.11.8"

isSnapshot := true

scalacOptions := Seq("-feature", "-unchecked", "-deprecation", "-encoding", "utf8")

val akkaVersion = "2.5.6"
val json4sVersion = "3.3.0"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion % "provided",
  "joda-time" % "joda-time" % "2.8.2" % "provided",
  "org.joda" % "joda-convert" % "1.7" % "provided",
  "org.json4s" %% "json4s-native" % json4sVersion % "provided"
)

scalacOptions in Test ++= Seq("-Yrangepos")
